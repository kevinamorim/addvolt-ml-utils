from setuptools import setup, find_packages

setup(
    name="addvolt_ml_utils",
    version="0.0.5",
    author='Leonel Araujo',
    author_email='leonel.araujo@addvolt.com',
    license="BSD",
    packages=find_packages(),
    install_requires=[
        'pandas==0.23.4',
        'geos==0.2.1',
        'Shapely==1.6.4.post1'
    ]
)
