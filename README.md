## addvolt-ml-utils

Utility functions to be used across data processing and machine learning projects.

This aims to standardize what AddVolt considers as "invalid data" and to enforce rules on data. For instance,
when using this project's `default_data_cleaning` method, all assumptions regarding what should be considered a
valid value for `rpm` or `speed_can` are already taken into account.

This library is only available for `python3`.


## Usage
`addvolt_ml_utils` provides an intuitve api to handle different tasks.

For example, to clean data:
```python3
from addvolt_ml_utils import preprocessing

df = pd.read_csv('data.csv')
df = preprocessing.default_data_cleaning(df)
df = preprocessing.maps_data_cleaning(df)

# df is now free of rpm, delta_t, etc. invalid values. The operations performed
# can be consulted in each function's documentation. 
```

Or to add some more columns:
```python3
from addvolt_ml_utils import preprocessing

df = pd.read_csv('data.csv')
df = preprocessing.refrigeration.add_refrigeration_columns(df)

# df now has additional columns that provide extra information, for example 'refrigeration_is_on'
# or 'refrigeration_electric_is_on'. Beware of some operations made by these functions, they may
# require some columns to exist. These constraints can also be consulted in each function's 
# documentation. 
```

#### Dependencies
As explicitly declared in `requirements.txt` file, this project strictly relies on `pandas` and `numpy`.

These can be installed by running `pip3 install -r requirements.txt`.

#### Installing
To install `addvolt_ml_utils` simply run `python3 setup.py install`.

Depending on whether you're using a virtual environment, this should install the package in python's packages path.

Alternatively, if you have `pip3` installed you can run `pip3 install .`.

#### Test
A test suite is included in `test` folder. Feel free to contribute with more tests.
To run the test suite: `python3 test/index.py`. Optionally, add flag `--verbose`.

To know the coverage of the tests using `coverage` package (`pip3 install coverage` may be needed):

1. Run `coverage run --branch test/index.py`. This will run the tests and measure its coverage using the parameters specified
in the `.coveragerc` file. It will store the results in `.coverage` file.
2. Run `coverage html`. This will create a `htmlcov` directory containing a graphic interface to show the coverage data.
    1. To open the data, run `htmlcov/index.html` on a browser, i.e. `google-chrome htmlcov/index.html`.
    2. Alternatively, run  `coverage report` to get a quick look directly in the command line.
    

#### Documentation
Documentation of this library can be generated using `sphinx`. To this end, the docs folder of the repo already
includes configuration files for generating HTML docs.

To generate locally:

1. Navigate to Run `docs` folder.
2. Run `sphinx-apidoc ../addvolt_ml_utils --force -o source/` to update the documentation layout.
3. Run `make html` to generate a html website with the docs. These pages are created in `_build` folder.
    1. To open the docs, run `docs/_build/html/index.html` on a browser, i.e. `google-chrome _build/html/index.html` 
    from the current directory.
