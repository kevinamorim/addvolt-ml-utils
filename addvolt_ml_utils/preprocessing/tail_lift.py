def add_tail_lift_columns(df, field='timer_digital_3'):
    """
    Adds tail_lift flag columns:

    - tail_lift_is_on: If tail lift is on or not. This is estimated since we only receive a value every 30 seconds.
    - tail_lift_startup: Tail lift's diesel model is starting up.
    - tail_lift_operating_timer: Tail lift's timer sum.

    :param df: Original dataframe that must have the field passed as parameter.
    :type df: pd.DataFrame
    :param field: Field in the dataframe with the tail lift timer
    :type field: string
    :return: Dataframe with additional 'tail_lift_is_on', 'tail_lift_startup' and 'tail_lift_operating_timer' columns.
    :rtype: pd.DataFrame
    """
    # If the specified field is not found, we will return everything as if the unit never worked.
    if field not in df:
        print('Field {} not found in the dataframe.'.format(field))
        df[field] = 0

    # When the field of the tail lift unit has a value, set the operating flag to True
    df.ix[df[field] >= 0, 'tail_lift_is_on'] = df[field] > 0

    # Fill lines without information on tail lift with 'backward fill' method
    df['tail_lift_is_on'] = df['tail_lift_is_on'].fillna(method='bfill')

    # Count the amount of times the tail lift unit was started
    df['tail_lift_startup'] = (df['tail_lift_is_on']) & (df['tail_lift_is_on'].shift() == False)

    # Cloning tail_lift_field to a column we programatically know for calculations instead of using the variable
    df['tail_lift_operating_timer'] = df[field]

    return df
