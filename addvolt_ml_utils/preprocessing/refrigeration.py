def add_refrigeration_columns(df, field='timer_digital_2'):
    """
    Adds refrigeration flag columns:

    - refrigeration_is_on: If refrigeration unit is on or not. This is estimated since we only receive a value every 30 seconds.
    - refrigeration_diesel_is_on: Refrigeration unit's diesel mode is on or not. This is estimated since we only receive a value every 30 seconds.
    - refrigeration_diesel_startup: Refrigeration unit's diesel model is starting up.
    - refrigeration_diesel_timer: Refrigeration unit's diesel timer. Since we only receive a value every 30 seconds, most rows will be empty but the sum will reflect total usage.
    - refrigeration_electric_is_on: Refrigeration unit's electric mode is on or not. Based on 'battery_current'.
    - refrigeration_electric_timer: Refrigeration unit's electric mode timer. Based on 'battery_current'.
    - refrigeration_operating_timer: Refrigeration unit's timer sum.

    :param df: Original dataframe that must have the field passed as parameter and registered_at
    :type df: pd.DataFrame
    :param field: Field in the dataframe with the refrigeration unit timer
    :type field: string
    :return: Dataframe with additional 'refrigeration_is_on', 'refrigeration_diesel_is_on', 'refrigeration_diesel_startup', 'refrigeration_diesel_timer', 'refrigeration_electric_is_on' and 'refrigeration_electric_timer' columns.
    :rtype: pd.DataFrame
    """
    # If the specified field is not found, we will return everything as if the unit never worked.
    if field not in df:
        print('Field {} not found in the dataframe.'.format(field))
        df[field] = 0

    if 'delta_t' not in df:
        df['delta_t'] = (df['registered_at'] - df['registered_at'].shift()).dt.total_seconds()
        df['delta_t'] = df['delta_t'] * (df['delta_t'] < 60) * (df['delta_t'] >= 0)

    # Diesel
    df.ix[df[field] >= 0, 'refrigeration_diesel_is_on'] = df[field] > 0  # Filling rows that explicitly tell us diesel mode fridge is ON or OFF
    df['refrigeration_diesel_is_on'] = df['refrigeration_diesel_is_on'].fillna(method='bfill')  # Filling in the rest of the rows wih backward fill
    df['refrigeration_diesel_timer'] = df[field]
    df['refrigeration_diesel_startup'] = (df['refrigeration_diesel_is_on']) & (df['refrigeration_diesel_is_on'].shift() == False)  # If we're switching from an OFF state to ON then we found a startup moment.

    # Electric
    df['refrigeration_electric_is_on'] = False
    df['refrigeration_electric_is_on_from_battery'] = False
    df['refrigeration_electric_is_on_from_grid'] = False
    df['refrigeration_electric_timer'] = 0

    # Electric from battery
    if 'battery_voltage' in df and 'battery_current' in df and 'delta_t' in df:
        df['battery_voltage'] = df['battery_voltage'].astype(float)
        df['battery_current'] = df['battery_current'].astype(float)

        # P(battery) = V battery * I battery
        df['battery_power'] = df.battery_voltage * df.battery_current

        df['refrigeration_electric_is_on_from_battery'] = (df['battery_current'] < -2) & (df['battery_current'] > -201)

    # Electric from grid
    if 'grid_available' in df and 'delta_t' in df:
        df['refrigeration_electric_is_on_from_grid'] = (df['grid_available'] == 3) & (df['refrigeration_electric_is_on_from_battery'] == False)

    df['refrigeration_electric_is_on'] = (df['refrigeration_electric_is_on_from_battery'] | df['refrigeration_electric_is_on_from_grid'])
    df['refrigeration_electric_timer'] = df['refrigeration_electric_is_on'] * df['delta_t']

    # Totals
    df['refrigeration_is_on'] = (df['refrigeration_diesel_is_on'] | df['refrigeration_electric_is_on'])
    df['refrigeration_operating_timer'] = (df['refrigeration_is_on'] * df['delta_t'])
    return df
