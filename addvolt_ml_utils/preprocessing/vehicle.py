def add_vehicle_columns(df, rpm_threshold=300):
    """
    Adds vehicle operation flag columns:

    - vehicle_is_on: Vehicle is operating
    - vehicle_is_driving: Vehicle is moving
    - vehicle_is_idle: Vehicle is idle
    - vehicle_is_stopped: Vehicle is stopped

    :param df: Original dataframe that must have the "rpm" and "speed_can" columns.
    :type df: pd.DataFrame
    :param rpm_threshold: Threshold to consider the vehicle in 'operating' mode
    :type rpm_threshold: int
    :return: Dataframe with additional 'vehicle_is_on', 'vehicle_is_driving', 'vehicle_is_idle' and 'vehicle_is_stopped' columns.
    :rtype: pd.DataFrame
    """
    # Setting vehicle state flag columns
    df['vehicle_is_on'] = df.rpm > rpm_threshold
    df['vehicle_is_driving'] = (df.speed_can > 0) & (df.vehicle_is_on == True)
    df['vehicle_is_idle'] = (df.speed_can == 0) & (df.vehicle_is_on == True)
    df['vehicle_is_stopped'] = ~ df.vehicle_is_on

    return df
