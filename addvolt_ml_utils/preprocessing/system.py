def add_system_columns(df):
    """
    Adds system flag columns:

    - system_battery_soc: State of Charge of the battery. Received periodically so bfill is applied.
    - system_is_on: System is currently on.
    - system_charging: System is charging.
    - system_discharging: System is discharging.

    :param df: Original dataframe.
    :type df: pd.DataFrame
    :return: Dataframe with additional 'system_battery_soc', 'system_is_on', 'system_charging' and 'system_discharging' columns.
    :rtype: pd.DataFrame
    """
    df['system_battery_soc'] = df['battery_soc']
    df['system_battery_soc'] = df['system_battery_soc'].fillna(method='bfill').fillna(method='ffill')
    df['system_is_on'] = False  # TODO: Not implemented yet
    df['system_charging'] = False
    df['system_discharging'] = False

    # Calculating battery power to determine if system is charging/discharging
    if 'battery_current' in df:
        df['system_charging'] = (df['battery_current'] > 1)
        df['system_discharging'] = (df['battery_current'] < -2)
        df['system_is_on'] = False  # TODO: Not implemented yet

    return df
