import unittest
import datetime
from addvolt_ml_utils.utils import format_duration


class FormatDurationTest(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_valid_parameters_only_days(self):
        timedelta = (datetime.datetime(2017, 11, 3) - datetime.datetime(2017, 11, 1)).total_seconds()
        formatted_str = format_duration(timedelta)
        self.assertTrue(formatted_str == '2day(s)')

    def test_valid_parameters_only_hours(self):
        timedelta = (datetime.datetime(2017, 11, 1, 10) - datetime.datetime(2017, 11, 1, 7)).total_seconds()
        formatted_str = format_duration(timedelta)
        self.assertTrue(formatted_str == '3h')

    def test_valid_parameters_only_minutes(self):
        timedelta = (datetime.datetime(2017, 11, 1, 10) - datetime.datetime(2017, 11, 1, 9, 30)).total_seconds()
        formatted_str = format_duration(timedelta)
        self.assertTrue(formatted_str == '30m')

    def test_valid_parameters_only_seconds(self):
        timedelta = (datetime.datetime(2017, 11, 1, 1) - datetime.datetime(2017, 11, 1, 0, 59, 45)).total_seconds()
        formatted_str = format_duration(timedelta)
        self.assertTrue(formatted_str == '15s')

    def test_valid_parameters_all_components(self):
        timedelta = (datetime.datetime(2017, 11, 2, 12) - datetime.datetime(2017, 11, 1, 9, 30, 20)).total_seconds()
        formatted_str = format_duration(timedelta)
        self.assertTrue(formatted_str == '1day(s) 2h 29m 40s')

    def test_no_duration(self):
        formatted_str = format_duration(0)
        self.assertTrue(formatted_str == '0m 0s')

    def test_negative_seconds(self):
        with self.assertRaises(BaseException):
            format_duration(-1)

if __name__ == '__main__':
    unittest.main()
