import unittest
import pandas as pd
from addvolt_ml_utils.preprocessing.photovoltaic import add_photovoltaic_columns


class AddSystemFlagsTest(unittest.TestCase):
    def setUp(self):
        data = dict(
            registered_at=pd.date_range('05/05/2017', periods=100, freq='1s'),
        )
        self.df = pd.DataFrame(data=data)
        pass

    def tearDown(self):
        pass

    def test_always_on(self):
        df = self.df.copy()
        df['current1'] = -2
        df['voltage1'] = 2
        result = add_photovoltaic_columns(df)
        self.assertEqual(len(result), df.shape[0])
        self.assertTrue(result['photovoltaic_current'].iloc[0] == df['current1'].iloc[0])
        self.assertTrue(result['photovoltaic_voltage'].iloc[0] == df['voltage1'].iloc[0])
        self.assertTrue(result['photovoltaic_energy_produced'].iloc[0] == 4)


if __name__ == '__main__':
    unittest.main()
