import unittest
import pandas as pd
import numpy as np
from addvolt_ml_utils.preprocessing import default_data_cleaning
import datetime


class DefaultDataCleaningTest(unittest.TestCase):
    def setUp(self):
        data = dict(
            registered_at=pd.date_range('05/05/2017', periods=100, freq='1s'),
            latitude=np.arange(40, 41, 0.01),
            longitude=np.arange(-9, -8, 0.01),
            rpm=np.random.randint(0, 3000, 100)
        )
        self.df = pd.DataFrame(data=data)

    def tearDown(self):
        pass

    def test_default_data_cleaning_keeps_columns(self):
        df = default_data_cleaning(self.df)

        columns = df.columns.tolist()
        self.assertTrue('registered_at' in columns)
        self.assertTrue('latitude' in columns)
        self.assertTrue('longitude' in columns)
        self.assertTrue('rpm' in columns)

    def test_default_data_cleaning_discards_impossible_rpm(self):
        # Creating a dataframe with 30 invalid rpm values
        df_with_invalid_rpm = self.df.copy()
        df_with_invalid_rpm.loc[0:29, 'rpm'] = np.random.randint(5000, 6000, 30)
        df_with_invalid_rpm.loc[30:100, 'rpm'] = np.random.randint(0, 3000, 70)
        df_with_invalid_rpm = default_data_cleaning(df_with_invalid_rpm)

        columns = df_with_invalid_rpm.columns.tolist()
        self.assertTrue('rpm' in columns)
        self.assertTrue(df_with_invalid_rpm[df_with_invalid_rpm.rpm > 5000].empty)

    def test_default_data_cleaning_discards_invalid_timestamps(self):
        # Creating a dataframe with absurd timestamp in between good values
        df_with_invalid_timestamp = self.df.copy()
        df_with_invalid_timestamp.loc[2, 'registered_at'] = datetime.datetime(2000, 1, 1)
        df_with_invalid_timestamp = default_data_cleaning(df_with_invalid_timestamp)

        columns = df_with_invalid_timestamp.columns.tolist()
        self.assertTrue('registered_at' in columns)
        self.assertTrue(df_with_invalid_timestamp[df_with_invalid_timestamp.delta_t > 5000].empty)

    def test_default_data_cleaning_flags_rows_with_invalid_gps(self):
        # Inserting absurd/invalid gps values
        df_with_invalid_gps = self.df.copy()
        df_with_invalid_gps.loc[2, 'latitude'] = -300
        df_with_invalid_gps.loc[2, 'longitude'] = -300
        df_with_invalid_gps.loc[7, 'latitude'] = -300
        df_with_invalid_gps.loc[7, 'longitude'] = -300

        df_with_invalid_gps = default_data_cleaning(df_with_invalid_gps)

        columns = df_with_invalid_gps.columns.tolist()
        self.assertTrue('latitude' in columns)
        self.assertTrue('longitude' in columns)
        self.assertTrue(df_with_invalid_gps.valid_gps.sum() == self.df.shape[0] - 4)

    def test_default_data_cleaning_removes_rows_with_invalid_gps_2(self):
        # Inserting absurd/invalid gps values
        df_with_invalid_gps = self.df.copy()
        df_with_invalid_gps.loc[2, 'latitude'] = -300
        df_with_invalid_gps.loc[2, 'longitude'] = -300
        df_with_invalid_gps.loc[5, 'latitude'] = 0
        df_with_invalid_gps.loc[5, 'longitude'] = 0
        df_with_invalid_gps.loc[6, 'latitude'] = -300
        df_with_invalid_gps.loc[6, 'longitude'] = -300

        df_with_invalid_gps = default_data_cleaning(df_with_invalid_gps)

        columns = df_with_invalid_gps.columns.tolist()
        self.assertTrue('latitude' in columns)
        self.assertTrue('longitude' in columns)
        self.assertTrue(df_with_invalid_gps.valid_gps.sum() == self.df.shape[0] - 5)

    def test_default_data_cleaning_removes_rows_with_invalid_gps_3(self):
        # Inserting absurd/invalid gps values
        df_with_invalid_gps = self.df.copy()
        df_with_invalid_gps.loc[2, 'latitude'] = -300
        df_with_invalid_gps.loc[2, 'longitude'] = -300
        df_with_invalid_gps.loc[3, 'latitude'] = 0
        df_with_invalid_gps.loc[3, 'longitude'] = 0
        df_with_invalid_gps.loc[4, 'latitude'] = -300
        df_with_invalid_gps.loc[4, 'longitude'] = -300
        df_with_invalid_gps.loc[6, 'latitude'] = df_with_invalid_gps.loc[5, 'latitude'] + 0.5
        df_with_invalid_gps.loc[6, 'longitude'] = df_with_invalid_gps.loc[5, 'longitude'] + 0.5

        df_with_invalid_gps = default_data_cleaning(df_with_invalid_gps)

        columns = df_with_invalid_gps.columns.tolist()
        self.assertTrue('latitude' in columns)
        self.assertTrue('longitude' in columns)
        self.assertTrue(df_with_invalid_gps.valid_gps.sum() == self.df.shape[0] - 6)

    def test_timer_connection_lost(self):
        # Creating a dataframe with 30 invalid rpm values
        data1 = dict(
            registered_at=pd.date_range('05/05/2017', periods=100, freq='1s'),
            latitude=np.arange(40, 41, 0.01),
            longitude=np.arange(-9, -8, 0.01),
            rpm=np.random.randint(0, 3000, 100)
        )

        data2 = dict(
            registered_at=pd.date_range('05/05/2017 00:05:00', periods=100, freq='1s'),
            latitude=np.arange(40, 41, 0.01),
            longitude=np.arange(-9, -8, 0.01),
            rpm=np.random.randint(0, 3000, 100)
        )

        df_with_connection_lost = pd.concat([pd.DataFrame(data=data1), pd.DataFrame(data=data2)])
        df_with_connection_lost = default_data_cleaning(df_with_connection_lost)

        self.assertTrue(df_with_connection_lost.timer_connection_lost.sum() > 150)

if __name__ == '__main__':
    unittest.main()
