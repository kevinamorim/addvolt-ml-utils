import unittest
import pandas as pd
from addvolt_ml_utils.preprocessing.pto import add_pto_threshold_columns


class AddPTOThresholdFlagsTest(unittest.TestCase):
    def setUp(self):
        data = dict(
            registered_at=pd.date_range('05/05/2017', periods=100, freq='1s'),
            speed_can=0
        )
        self.df = pd.DataFrame(data=data)
        pass

    def tearDown(self):
        pass

    def test_always_on(self):
        df = self.df.copy()
        df['rpm'] = 1200
        result = add_pto_threshold_columns(df)
        self.assertEqual(len(result), df.shape[0])
        self.assertEqual(result['pto_is_on'].value_counts()[True], result.shape[0])  # Every value is True
        self.assertEqual(result['pto_startup'].value_counts()[False], result.shape[0])  # Every value is False
        self.assertEqual(sum(result['pto_operating_timer']), df.shape[0])

    def test_always_off(self):
        df = self.df.copy()
        df['rpm'] = 200
        result = add_pto_threshold_columns(df)
        self.assertEqual(len(result), df.shape[0])
        self.assertEqual(result['pto_is_on'].value_counts()[False], result.shape[0])  # Every value is True
        self.assertEqual(result['pto_startup'].value_counts()[False], result.shape[0])  # Every value is False
        self.assertEqual(sum(result['pto_operating_timer']), 0)

    def test_check_parameter_field(self):
        df = self.df.copy()
        df['rpm'] = 800
        result = add_pto_threshold_columns(df, threshold=600)
        self.assertEqual(len(result), df.shape[0])
        self.assertEqual(result['pto_is_on'].value_counts()[True], result.shape[0])  # Every value is True
        self.assertEqual(result['pto_startup'].value_counts()[False], result.shape[0])  # Every value is False
        self.assertEqual(sum(result['pto_operating_timer']), df.shape[0])

    def test_3_startups(self):
        df = self.df.copy()
        df['rpm'] = 900
        df.loc[11:20, 'rpm'] = 1100
        df.loc[41:50, 'rpm'] = 1100
        df.loc[71:80, 'rpm'] = 1100

        result = add_pto_threshold_columns(df)
        self.assertEqual(len(result), df.shape[0])
        # To every work time (10 sec counting from rows 11, 41 and 71) we add 5 seconds of filter. So 3 * (10 + 5) = 45
        self.assertEqual(result['pto_is_on'].value_counts()[True], 45)
        self.assertEqual(result['pto_is_on'].value_counts()[False], 55)
        self.assertEqual(result['pto_operating_timer'].sum(), 45)
        self.assertEqual(result['pto_startup'].value_counts()[True], 3)

if __name__ == '__main__':
    unittest.main()
