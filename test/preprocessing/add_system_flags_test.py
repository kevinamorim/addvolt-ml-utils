import unittest
import pandas as pd
from addvolt_ml_utils.preprocessing.system import add_system_columns


class AddSystemFlagsTest(unittest.TestCase):
    def setUp(self):
        data = dict(
            registered_at=pd.date_range('05/05/2017', periods=100, freq='1s'),
        )
        self.df = pd.DataFrame(data=data)
        pass

    def tearDown(self):
        pass

    def test_battery_soc(self):
        df = self.df.copy()
        df['battery_soc'] = 50

        result = add_system_columns(df)
        self.assertEqual(len(result), df.shape[0])
        self.assertTrue(result['system_battery_soc'].iloc[0] == 50)

    def test_system_charging(self):
        df = self.df.copy()
        df['battery_soc'] = 50

        df.loc[1:10, 'battery_current'] = -10
        df.loc[41:60, 'battery_current'] = -10

        result = add_system_columns(df)
        self.assertEqual(len(result), df.shape[0])
        self.assertTrue(result['system_battery_soc'].iloc[0] == 50)
        self.assertTrue(result['system_discharging'].sum() == 30)
        self.assertTrue(result['system_charging'].sum() == 0)

    def test_system_discharging(self):
        df = self.df.copy()
        df['battery_soc'] = 50

        df.loc[1:10, 'battery_current'] = 10
        df.loc[41:60, 'battery_current'] = 10

        result = add_system_columns(df)
        self.assertEqual(len(result), df.shape[0])
        self.assertTrue(result['system_battery_soc'].iloc[0] == 50)
        self.assertTrue(result['system_discharging'].sum() == 0)
        self.assertTrue(result['system_charging'].sum() == 30)


if __name__ == '__main__':
    unittest.main()
