import unittest
import datetime
import pandas as pd
from addvolt_ml_utils.preprocessing.operation import add_facilities_flags


class AddFacilitiesFlagsTest(unittest.TestCase):
    def setUp(self):
        # Create a simple square in a geojson
        self.facilities = {
            "type": "FeatureCollection",
            "features": [
                {
                    "type": "Feature",
                    "geometry": {
                        "type": "Polygon",
                        "coordinates": [
                            [[0, 0], [1, 0], [1, 1], [0, 1], [0, 0]]
                        ]
                    }
                }
            ]
        }

        # Create a geojson with two squares
        self.multiple_facilities = {
            "type": "FeatureCollection",
            "features": [
                {
                    "type": "Feature",
                    "geometry": {
                        "type": "Polygon",
                        "coordinates": [
                            [[0, 0], [1, 0], [1, 1], [0, 1], [0, 0]]
                        ]
                    }
                },
                {
                    "type": "Feature",
                    "geometry": {
                        "type": "Polygon",
                        "coordinates": [
                            [[2, 0], [3, 0], [3, 1], [2, 1], [2, 0]]
                        ]
                    }
                }
            ]
        }

    def tearDown(self):
        pass

    def test_empty_dataframe(self):
        """When passing an empty dataframe, an error should be thrown"""
        with self.assertRaises(Exception):
            df = add_facilities_flags(pd.DataFrame(), facilities=self.facilities)

    def test_empty_df_and_facilities(self):
        """When passing an empty dataframe, an error should be thrown"""
        with self.assertRaises(Exception):
            df = add_facilities_flags(pd.DataFrame(), facilities=dict())

    def test_empty_facilities(self):
        """When passing an empty dataframe, an error should be thrown"""
        df = pd.DataFrame([
            [datetime.datetime(2017, 1, 1, 10, 0), -1, -1],  # Still outside
            [datetime.datetime(2017, 1, 1, 10, 1), 0.5, 0.5],  # Inside
            [datetime.datetime(2017, 1, 1, 10, 2), 1, 1],  # On the border
            [datetime.datetime(2017, 1, 1, 10, 3), 2, 2]  # Outside
        ], columns=['registered_at', 'latitude', 'longitude'])

        df = add_facilities_flags(df, facilities=dict())
        self.assertTrue(df['is_in_facilities'].value_counts()[False] == df.shape[0])
        self.assertTrue(df['entered_facilities'].value_counts()[False] == df.shape[0])

    def test_should_trigger_default(self):
        """Should trigger when there's a value inside the boundaries."""
        df = pd.DataFrame([
            [datetime.datetime(2017, 1, 1, 10, 0), -1, -1],  # Still outside
            [datetime.datetime(2017, 1, 1, 10, 1), 0.5, 0.5],  # Inside
            [datetime.datetime(2017, 1, 1, 10, 2), 1, 1],  # On the border
            [datetime.datetime(2017, 1, 1, 10, 3), 2, 2]  # Outside
        ], columns=['registered_at', 'latitude', 'longitude'])

        df = add_facilities_flags(df, facilities=self.facilities)
        self.assertTrue(df['is_in_facilities'].value_counts()[True] == 1)
        self.assertTrue(df['entered_facilities'].value_counts()[True] == 1)

    def test_should_not_trigger_boundaries(self):
        """Should not trigger when the values are on the boundaries but not inside."""
        df = pd.DataFrame([
            [datetime.datetime(2017, 1, 1, 10, 0), -1, -1],  # Outside
            [datetime.datetime(2017, 1, 1, 10, 1), 0, 0],  # On the border
            [datetime.datetime(2017, 1, 1, 10, 2), 1, 1],  # On the border
            [datetime.datetime(2017, 1, 1, 10, 3), 2, 2]  # Outside
        ], columns=['registered_at', 'latitude', 'longitude'])

        df = add_facilities_flags(df, facilities=self.facilities)
        self.assertTrue(df['is_in_facilities'].value_counts()[False] == 4)
        self.assertTrue(df['entered_facilities'].value_counts()[False] == 4)

    def test_should_trigger_default_two_features(self):
        """Should trigger when there's a value inside the boundaries."""
        df = pd.DataFrame([
            [datetime.datetime(2017, 1, 1, 10, 0), 0.5, -0.5],  # Outside
            [datetime.datetime(2017, 1, 1, 10, 1), 0.5, 0],  # On the border
            [datetime.datetime(2017, 1, 1, 10, 2), 0.5, 0.5],  # Inside
            [datetime.datetime(2017, 1, 1, 10, 3), 0.5, 1],  # On the border
            [datetime.datetime(2017, 1, 1, 10, 4), 0.5, 1.5],  # Outside
            [datetime.datetime(2017, 1, 1, 10, 5), 0.5, 2],  # Border
            [datetime.datetime(2017, 1, 1, 10, 6), 0.5, 2.5],  # Inside
            [datetime.datetime(2017, 1, 1, 10, 6), 0.5, 3],  # Border
            [datetime.datetime(2017, 1, 1, 10, 6), 0.5, 3.5],  # Outside
        ], columns=['registered_at', 'latitude', 'longitude'])

        df = add_facilities_flags(df, facilities=self.multiple_facilities)
        self.assertTrue(df['is_in_facilities'].value_counts()[True] == 2)
        self.assertTrue(df['entered_facilities'].value_counts()[True] == 2)

    def test_should_not_trigger_default_two_features(self):
        """Should not trigger since there's no value inside boundaries."""
        df = pd.DataFrame([
            [datetime.datetime(2017, 1, 1, 10, 4), 0.5, 1.5],  # Outside
            [datetime.datetime(2017, 1, 1, 10, 5), 0.5, 2],  # Border
            [datetime.datetime(2017, 1, 1, 10, 6), 0.5, 3],  # Border
            [datetime.datetime(2017, 1, 1, 10, 6), 0.5, 3.5],  # Outside
        ], columns=['registered_at', 'latitude', 'longitude'])

        df = add_facilities_flags(df, facilities=self.multiple_facilities)
        self.assertTrue(df['is_in_facilities'].value_counts()[False] == 4)
        self.assertTrue(df['entered_facilities'].value_counts()[False] == 4)

if __name__ == '__main__':
    unittest.main()
